const express = require('express'),
		bodyParser = require('body-parser'),
		morgan = require('morgan'),
		mailer = require('./mailer'),
		app = express(),
		port = 8080;

const AWS = require('aws-sdk');

// Enter copied or downloaded access ID and secret key here
const ID = 'AKIAI475PDFKUBMTDWUA';
const SECRET = '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';

// The name of the bucket that you have created
const BUCKET_NAME = 'nesho-image-bucket';

const s3 = new AWS.S3({
    accessKeyId: ID,
    secretAccessKey: SECRET
});

const params = {
    Bucket: BUCKET_NAME,
    CreateBucketConfiguration: {
        // Set your region here
        LocationConstraint: "us-east-2"
    }
};


const uploadFile = (file) => {
    // Read content from the file
    const fileContent = file.src;

    // Setting up S3 upload parameters
    const params = {
        Bucket: BUCKET_NAME,
        Key: file.name, // File name you want to save as in S3
        Body: fileContent
    };

    // Uploading files to the bucket
    s3.upload(params, function(err, data) {
        if (err) {
            throw err;
        }
        console.log(`File uploaded successfully. ${data.Location}`);
    });
};


app.use(bodyParser.urlencoded({
	extended: true
}))

.use(bodyParser.json())

/* log requests to console */
.use(morgan('dev'));

app.post('/client/apply', (req, res) => {
	mailer(req.body, (wasSent) => {
		res.send(wasSent);
	});
});

app.post('/contact/message', (req, res) => {
	mailer(req.body, (wasSent) => {
		res.send(wasSent);
	});
});

app.post('/file/upload', (req, res) => {
	console.log(req.body);
});

module.exports = app;