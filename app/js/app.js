(function(){

  angular.module('templates', []);

  angular.module('app', [
  	'ui.router',
    'templates',
    'ngMessages',
    'ngAnimate',
    //Custom components
    'navigation',
    'header',
    'footer',
    'homePage',
    'contactUs',
    'about',
    'products',
    'application',
    'prodForm',
    'employment',
    'modal',
    'login',
    'adminDashboard',
    'carousel',
    'filter',
    //Services
    'ScrollingService',
    'SubmitService',
    'AuthService',
    'CookieService'
  ]);

  angular.module('app').config(appConfiguration);
  appConfiguration.$inject = ['$stateProvider', '$urlRouterProvider', '$compileProvider'];
  function appConfiguration ($stateProvider, $urlRouterProvider, $compileProvider) {

    $compileProvider.preAssignBindingsEnabled(true);
    $stateProvider
    
    .state('login', {
      url: '/login',
      template: '<login></login>'
    })
    .state('home', {
      resolve: {
        "check": function($location, $rootScope){
          if(!$rootScope.isLoggedIn){
            $location.path('/login');
          }
        }
      },
      url: '/',
      template: '<home-page></home-page>',
    })
    .state('about', {
      resolve: {
        "check": function($location, $rootScope){
          if(!$rootScope.isLoggedIn){
            $location.path('/login');
          }
        }
      },
      url: '/about',
      template: '<about></about>'
    })
    .state('pictures', {
      resolve: {
        "check": function($location, $rootScope){
          if(!$rootScope.isLoggedIn){
            $location.path('/login');
          }
        }
      },
      url: '/products',
      template: '<products></products>'
    })
    .state('apply', {
      resolve: {
        "check": function($location, $rootScope){
          if(!$rootScope.isLoggedIn){
            $location.path('/login');
          }
        }
      },
      url: '/apply',
      template: '<application></application>'
    })
    .state('employment', {
      resolve: {
        "check": function($location, $rootScope){
          if(!$rootScope.isLoggedIn){
            $location.path('/login');
          }
        }
      },
      url: '/employment',
      template: '<employment></employment>'
    })
    .state('cart', {
      resolve: {
        "check": function($location, $rootScope){
          if (!$rootScope.isLoggedIn) {
            $location.path('/login');
          }
        }
      },
      url: '/contact-us',
      template: '<contact-us></contact-us>'
    })
    .state('admin-dashboard', {
      resolve: {
        "check": function($location, $rootScope){
          if(!$rootScope.isLoggedIn || !$rootScope.isPresident){
            $location.path('/login');
            alert('Not Authorized');
          }
        }
      },
      url: '/admin-dashboard',
      template: '<admin-dashboard></admin-dashboard>'
    });


    $urlRouterProvider.otherwise('/');
  }

  angular.module('app').run(['AuthService', '$rootScope', function (AuthService) {
    AuthService.setUrl('test');
  }]);

})();
