(function () {
  'use strict';
  angular.module('login')
  .directive('login', function () {
    return {
      restrict: 'E',
      templateUrl: 'app/js/components/login/login.html',
      controller: 'loginController as login'
    };
  });
})();
