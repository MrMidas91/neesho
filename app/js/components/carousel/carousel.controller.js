(function () {
	angular.module('carousel')
		.controller('carouselController', controller);

	controller.$inject = ['$scope'];	
	function controller ($scope) {
		var vm = this;
		vm.picIndex = 0;
		vm.imagePaths = $scope.images;
		vm.isManual = $scope.manual;

		console.log($scope, vm);
		if (!$scope.manual) {
			setInterval( () => {
				incrementPhotos();
			}, 3000);
		}

		vm.increment = incrementPhotos;
		vm.decrement = function(){
			if (!vm.picIndex) {
				vm.picIndex = vm.imagePaths.length - 1;
			} else {
				vm.picIndex--;
			}

			vm.currentPath = vm.imagePaths[vm.picIndex];
			$scope.$digest();
		}


		function incrementPhotos() {
			if (vm.picIndex === vm.imagePaths.length - 1) {
				vm.picIndex = 0;
			} else {
				vm.picIndex++;
			}

			vm.currentPath = vm.imagePaths[vm.picIndex];
			$scope.$digest();
		}
	};
})();