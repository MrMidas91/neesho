(function () {
  'use strict';
  angular.module('carousel')
  .directive('carousel', function () {
    return {
      restrict: 'E',
      templateUrl: 'app/js/components/carousel/carousel.html',
      controller: 'carouselController as carousel',
      scope: {
        header: '=',
        images: '=',
        manual: '='
      }
    };
  });
})();
