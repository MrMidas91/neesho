(function () {
  'use strict';
  angular.module('products')
  .service('productsService', service);

  service.$inject = ['$q', '$http'];
  function service ($q, $http) {
    const service = this;
    const apiBaseUrl = 'https://qb3chnoeze.execute-api.us-east-2.amazonaws.com/Production/';
    service.imgBaseUrl = 'https://green-valley-image-bucket.s3.us-east-2.amazonaws.com/';


    AWS.config.region = 'us-east-2';

    AWS.config.update({ accessKeyId: 'AKIAI475PDFKUBMTDWUA', secretAccessKey: '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' });

    $http.get(apiBaseUrl + 'login')
      .then((res) => {
        AWS.config.update({ accessKeyId: res.data.accessKeyId, secretAccessKey: res.data.secretAccessKey });
        bucket = new AWS.S3({ params: { Bucket: 'green-valley-image-bucket', maxRetries: 10 }, httpOptions: { timeout: 360000 } });
      })
      .catch((e) => {
        console.log(e);
      });

      service.subCategories = [];

      service.items = [];

    $http.get(apiBaseUrl + 'categories')
      .then((res) => {
        res.data.categories.forEach((node) => {
          switch(node.type) {
            case 'sub':
              service.subCategories.push(node.name);
              break;
            default:
              service.items.push(node.name);
            break;
          }
        });
      })
      .catch((e) => {
        console.log(e);
      });

    const collection = [];

    const products =[];

    let productToAdd = {

    };

    $http.get(apiBaseUrl + 'products')
      .then((res) => {
        console.log(res);
        const prods = res.data.products;
        prods.forEach((prod) => {
          products.push(prod);
        });
      })
      .catch((e) => {
        console.log(e);
      });

    service.getCollection = () => {
      return collection;
    }

    service.addToCollection = (item) => {
      collection.push(item);
    }

    service.setProductToAdd = function (pta) {
      let newObjToSet = {}
      Object.keys(pta).forEach((key) => {
        if (key !== 'potentialServing') {
          newObjToSet[key] = pta[key]
        }
      });
      productToAdd = newObjToSet;

      if (!productToAdd.hasOwnProperty('images')) {
        productToAdd.images = [];
      }
    }

    service.removeItemFromCollection = (item) => {
      const indexToRemove = collection.indexOf(item);
      if (indexToRemove !== -1 ) {
        collection.splice(indexToRemove, 1);
      }
    }

    service.removeProduct= (item) => {
      $http.delete(`${apiBaseUrl}/products?name=${item.name}`);
      const indexToRemove = products.indexOf(item);
      products.splice(indexToRemove, 1);
      service.removeItemFromCollection(item);
    }

    service.addProduct = (item) => {
      return new Promise((resolve, reject) => {
        $http.post(`${apiBaseUrl}/products`, productToAdd)
        .then((data) => {
            products.push(productToAdd);
            productToAdd = {};
            resolve(true);
        })
        .catch((e) => {
          reject(e);
          console.log(e);
        });
      });
    }

  	service.getProducts = () => {
  		return products;
    }
    
    service.upload = (file) => {
        const deferred = $q.defer();
        const params = { Bucket: 'green-valley-image-bucket', Key: file.name, ContentType: file.type, Body: file.file };
        const options = {
            partSize: 10 * 1024 * 1024,
            queueSize: 1,
            ACL: 'bucket-owner-full-control'
        };
        const uploader = bucket.upload(params, options, (err, data) => {
            if (err) {
                deferred.reject(err);
            }
            console.log(data);
            if (!productToAdd.hasOwnProperty('images')) {
              productToAdd.images = [];
            }
            productToAdd.images.push(file.name);
            alert('file saved!');
            deferred.resolve();
        });
        uploader.on('httpUploadProgress', (event) => {
            deferred.notify(event);
        });

        return deferred.promise;
    }
  }
})();