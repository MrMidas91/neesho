(function () {
	angular.module('products')
		.controller('productsController', controller);

	controller.$inject = ['$scope', 'productsService', 'filterService', 'AuthService'];	
	function controller ($scope, productsService, filterService, AuthService) {
		var vm = this;
		vm.content = {
			title: 'The Reserve',
			about: 'AMAZING ASS PROTDUCTDAFA',
			description: 'AMAINGGG ASS PRODUCt'
		};

		vm.image = '';
		vm.name = '';

		vm.user = AuthService.getUser() || {access: 'gen'};

		vm.itemToInclude = filterService.getFilterSelection();

		vm.productIncluded = function (product) {
			return Object.values(product).some(val => vm.itemToInclude.includes(val));
		}

		vm.products =  productsService.getProducts();

		vm.collection = productsService.getCollection();

		vm.removeItemFromCollection = productsService.removeItemFromCollection;

		vm.addToCollection = productsService.addToCollection;

		vm.checkAvailability = function (product) {
			if (product.isLimited && vm.user.access === 'gen') {
				return false;
			}

			return true;
		}

		vm.closeModal = (e) => {
			e.stopPropagation();
			vm.isModalOpen = false;
			vm.selectedProductImage = '';
		}

		vm.openModal = (product) => {
			if (product.images && product.images.length) {
				vm.image = product.images[0];
				vm.name = product.name;
				vm.isModalOpen = true;
				console.log(vm.selectedProductImage);
			}
		};
	  
	};
})();