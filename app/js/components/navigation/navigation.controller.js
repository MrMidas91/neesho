(function () {
  'use strict';
  angular.module('navigation')
  .controller('navigationController', controller);

  controller.$inject = ['$state', '$rootScope','NavigationService', 'AuthService'];
  function controller ($state, $rootScope, NavigationService, AuthService) {
  	const vm = this;
    vm.menuSettings = NavigationService;

  	vm.toggleMenu = () => {
		  vm.menuSettings.isMenuOpen = !vm.menuSettings.isMenuOpen;
  	};

  	vm.navigate = (route) => {
  		$state.go(route);
  		vm.menuSettings.closeMenu();
  	};

  	vm.routes = [{
  		route: 'home',
  		format: 'Home',
  	}, {
  		route: 'pictures',
  		format: 'Pictures'
  	}];

	if ($rootScope.isLoggedIn && $rootScope.isPresident && vm.routes.length === 2 && AuthService.getUser().access === 'curator') {
		vm.routes.push({
			route: 'admin-dashboard',
			format: 'Dashboard'
		});
	}
  }
})();