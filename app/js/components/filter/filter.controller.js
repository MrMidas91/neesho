(function () {
	angular.module('filter')
		.controller('filterController', controller);

	controller.$inject = ['$scope', 'filterService', 'productsService'];	
	function controller ($scope, filterService, productsService) {
		var vm = this;
		
		vm.filterCategories = extractFromProduct(productsService.getProducts());
		vm.categories = Object.keys(vm.filterCategories);
		vm.model = {};

		vm.toggleFilterInclusion = (item) => {
			if (vm.filterSelection.includes(item)) {
				filterService.removeFilterSelection(item);
			} else {
				filterService.addToFilter(item);
			}
		}
		vm.filterSelection = filterService.getFilterSelection();

		function keyEquals(key) {
			return ['category', 'type', 'thc'].includes(key.toLowerCase());
		}

		function extractFromProduct(prods) {
			const prodsToReturn = {};
			prods.forEach((p) => {
				Object.keys(p).forEach(key => {
					if (!prodsToReturn.hasOwnProperty(key) && keyEquals(key)) {
						prodsToReturn[key] = [];
					}

					if (prodsToReturn.hasOwnProperty(key) && !prodsToReturn[key].includes(p[key]) && keyEquals(key)) {
						prodsToReturn[key].push(p[key]);
					}
					 
				});
			});
			return prodsToReturn;
		}
	};
})();