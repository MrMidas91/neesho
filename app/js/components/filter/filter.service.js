(function () {
	'use strict';
	angular.module('filter')
		.service('filterService', service);

	function service () {
		const service = this;
        
        const filterSelection = [];

        service.addToFilter = (item) => {
            filterSelection.push(item);
        };

        service.getFilterSelection = () => {
            return filterSelection;
        }

        service.removeFilterSelection = (item) => {
            const indexToRemove = filterSelection.indexOf(item);
            filterSelection.splice(indexToRemove, 1);
        }
	}
})();