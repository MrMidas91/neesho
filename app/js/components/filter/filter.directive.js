(function () {
  'use strict';
  angular.module('filter')
  .directive('filter', function () {
    return {
      restrict: 'E',
      templateUrl: 'app/js/components/filter/filter.html',
      controller: 'filterController as filter'
    };
  });
})();
