(function () {
    'use strict';
    angular.module('adminDashboard')
    .service('adminService', service);
  
    service.$inject = ['$q', '$http'];
    function service ($q, $http) {

        var service = this;
        const apiBaseUrl = 'https://qb3chnoeze.execute-api.us-east-2.amazonaws.com/Production/users';

        let clients = [];

        service.fetchClients = function () {
            return new Promise(function(resolve, reject) {
                $http.get(apiBaseUrl)
                    .then((res) => {
                        clients = res.data.clients;
                        resolve(res.data.clients);
                    })
                    .catch((e) => {
                        console.log(e);
                        resolve([]);
                    });
            });
        }

        service.createUser = function(user) {
            return new Promise(function(resolve, reject) {
                $http.post(apiBaseUrl, {
                    username: user.clientId,
                    access: user.type,
                    password: user.password
                }).then((res) => {
                    if (res.data.failure) {
                        reject(res.data.failure);
                        return res.data.failure;
                    } else {
                        clients.push({
                            clientId: user.clientId,
                            type: user.type,
                            password: user.password
                        });
                        resolve(res.data.message);
                        alert(res.data.message);
                    }
                }).catch((e) => {
                    const errorMessage = 'There was an error processing your request. Please close your browser and try again';
                    reject(errorMessage);
                    alert(errorMessage);
                });
            });
        }

        service.getClients = function() {
            return clients;
        }

        service.editClient = function(client) {
            return new Promise((resolve, reject) => {
                $http.put(apiBaseUrl,{
                    username: client.clientId,
                    access: client.type
                }).then((res) => {
                    if (res.data.hasOwnProperty('failure')) {
                        reject(res.data.failure);
                        alert(res.data.failure);
                    } else {
                        resolve('User successfully updated')
                        alert('User successfully updated');
                    }
                })
                .catch(e => {
                    console.log(e);
                    reject('unable to update admirer. please, try again');
                    alert('unable to update admirer. please, try again');
                });
            });
        }

        service.removeClient = function(client) {
            $http.delete(apiBaseUrl+ '?username=' + client.clientId)
            .catch((e) => {
                console.log(e);
            });
            const indexToRemove =  clients.indexOf(client);
            clients.splice(indexToRemove, 1);
        }

     }
  })();