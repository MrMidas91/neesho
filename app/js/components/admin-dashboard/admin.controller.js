(function () {
  'use strict';
  angular.module('adminDashboard')
  .controller('adminController', controller);

  controller.$inject = ['$scope','productsService', 'adminService', 'AuthService'];
  function controller ($scope, productsService, adminService, AuthService) {
  	const vm = this;

    vm.typeOptions = ['indica', 'hybrid', 'sativa'];
    vm.thcOptions = ['Diamond', 'Platinum', 'Gold', 'Silver'];
    vm.categoryOptions = ['Flower', 'Extracts', 'Vape'];
    vm.clientOptions = ['curator', 'lmr', 'gen'];
    vm.servings = ['Gram', 'Eighth'];
    vm.products = productsService.getProducts();
    vm.productConfigured = false;
    vm.isEdit = false;

    vm.removeProduct = productsService.removeProduct;
    vm.clients = [];

    vm.user = AuthService.getUser();

    adminService.fetchClients()
      .then(() => {
        vm.clients = adminService.getClients();
        $scope.$apply();
      });
    vm.subMenuOpen = false;
    vm.itemMenuOpen = false;
    vm.subcategories = productsService.subCategories;

    vm.items = productsService.items;

    vm.selectCategory = (cat) => {
      vm.form.subcategory = cat;
      console.log('its selected');
      vm.subCategoriesBlur();
    };

    vm.selectItem = (cat) => {
      vm.form.item = cat;
      vm.itemBlur();
    }

    vm.itemBlur = () => {
      vm.itemMenuOpen = false;
    }

    vm.itemFocus = () => {
      vm.itemMenuOpen = true;
    }

    vm.subCategoriesBlur = () => {
      vm.subMenuOpen = false;
    }

    vm.subCategoriesFocus = () => {
      vm.subMenuOpen = true;
    }

    vm.updateUser = function(user) {
      vm.isEdit = true;
      vm.editUser(user);
    }

    vm.editProduct = function (item) {
      vm.form = Object.assign({},vm.form, item); 
      $scope.$apply();
    }
    
    vm.cancelUpdate = function() {
      vm.userForm.type = '';
      vm.userForm.clientId = '';
      vm.userForm.password = '';
      vm.isEdit = false;
      $scope.$apply();
    }

    vm.configured = function() {
      vm.productConfigured = true;
      productsService.setProductToAdd(vm.form);
    }

    vm.addProduct = (prod) => {
      if (canSubmit(prod.name)) {
        productsService.addProduct(prod)
          .then(() =>{
            vm.form = {
              potentialServing: {},
              servings: []
            };
            vm.productConfigured = false;
            $scope.$digest();
          })
          .catch(() => {
            alert('there was an error adding your project. try again.');
          });
      } else {
        alert('Product with that name already exist')
      }
    }

    function canSubmit(name) {
      let nameIsIncluded = false;
      vm.products.forEach((item) => { 
        if (item.name === name) {
          nameIsIncluded = true;
        }
      });
      return !nameIsIncluded;
    }

    vm.form = {
      type: 'indica',
      price: 12,
      name: 'Blue Dream',
      thc: 'Silver',
      category: 'Flower',
      source: 'Cannabatix',
      serial: uuidv4('xxx-xxx-xxx'),
      isLimited: false,
      potentialServing: {
        serving:vm.servings[0],
        price: '25'
      },
      servings: []
    };

    vm.userForm = {
      type: vm.clientOptions[0],
      clientId: uuidv4(),
      password: uuidv4('xxxxxxxx'),
    };

    vm.addServing = function () {
      vm.form.servings.push({
        price: vm.form.potentialServing.price,
        amount: vm.form.potentialServing.serving
      });
      vm.form.potentialServing = {};
    }

    vm.removeServing = function (idx) {
      vm.form.servings.splice(idx, 1);
      console.log(vm.form.servings);
    }

    vm.generateNewId = function (field, format) {
      if (!field) {
        return uuidv4(format);
      }
      vm.userForm[field] = uuidv4(format);
    }

    vm.createUser = function () {
      adminService.createUser(vm.userForm)
        .then(() => {
          vm.userForm.type = '';
          vm.userForm.clientId = '';
          vm.userForm.password = '';
          $scope.$apply();
        });
    }

    vm.removeAdmirer = function (client) {
      adminService.removeClient(client);
    }

    vm.editUser = function (client) {
      vm.userForm.type = client.access;
      vm.userForm.clientId = client.clientId;
      vm.userForm.password = 'Password cannot be updated';
    }

    vm.submitEdit = function () {
      adminService.editClient(vm.userForm)
        .then(() => {
          for (let i = 0; i <= vm.clients.length; i++) {
            if (vm.clients[i].clientId === vm.userForm.clientId) {
              vm.clients[i].access = vm.userForm.type;
              vm.clients[i].type = vm.userForm.type;
              break;
            }
          }

          vm.userForm.type = '';
          vm.userForm.clientId = '';
          vm.userForm.password = '';
          $scope.$apply();
        })
        .catch(() => {
          console.log('error');
        });
    }

    function uuidv4(format) {
      let _format = format || 'xxx-xxx-xx-xx';
      return _format.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
      });
    }
  }
})();