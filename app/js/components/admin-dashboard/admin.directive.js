(function () {
	'use strict';
	angular.module('adminDashboard')
		.directive('adminDashboard', directive);

	function directive () {
		return {
			restrict: 'E',
			templateUrl: 'app/js/components/admin-dashboard/admin.html',
			controller: 'adminController as admin'
		}
	}
})();