(function () {
  'use strict';
  angular.module('contactUs')
  .controller('contactController', controller);

  controller.$inject = ['$state', '$scope', '$http', 'SubmitService', 'contactUsService', 'productsService'];
  function controller ($state, $scope, $http, SubmitService, contactUsService, productsService) {
  	const vm = this;
    vm.canCopy = false;
    vm.collection = productsService.getCollection();

    vm.setCanCopy = function() {
      vm.canCopy = true;
    }

    vm.copyToClipboard = function myFunction() {
      const el = document.createElement('textarea');
      el.value =  'Client:' + 23243 + ' ' + JSON.stringify(vm.collection);
      document.body.appendChild(el);
      el.select();
      document.execCommand('copy');
      document.body.removeChild(el);
      alert("Copied Your order to your clipboard!");
    }
  }
})();