(function () {
	'use strict';
	angular.module('contactUs')
		.service('contactUsService', service);

	function service () {
		const service = this;
        
        const cart = [];

        service.isCartEmpty = () => {
            return !cart.length;
        }

        service.addToCart = (item) => {
            cart.push(item);
        }

        service.removeFromCart = (item) => {
            cart.filter((_item) => {
                return item !== _item;
            });
        }
	}
})();