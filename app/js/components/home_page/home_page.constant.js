(function () {
	'use strict';
	angular.module('homePage')
		.constant('homePageContent', {
			title: 'The Reserve',
			description: 'The Reserve is a revolutionay company designed to create amazing products and services',
			about: 'About',
			psExplanation: `When we mention P&S(product and services) it extends beyond the idea of items.
			At The Reserve any brand under our covering is a product and what they provide is what we call
			the service. We have a wide variety of product each offering a different service. We have a desire
			to provide services to every person possible and for that very reason The Reserve was born.`
		});
})();