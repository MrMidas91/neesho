(function () {
	angular.module('homePage')
		.controller('homePageController', controller);

	controller.$inject = ['$scope', '$state','homePageContent', 'productsService'];	
	function controller ($scope, $state, homePageContent, productsService) {
		var vm = this;
		vm.content = homePageContent;
		vm.products = productsService.getProducts();

		vm.productCategories = [{
			name: 'Flower',
			images: [
				'app/images/flower-1.jpg',
				'app/images/flower-2.jpg',
				'app/images/flower-3.jpg',
				'app/images/flower-4.jpg',
			]
		}, {
			name: 'Concentrates',
			images: [
				'app/images/rove-1.jpg',
				'app/images/rove-2.jpg',
				'app/images/rove-3.jpg',
			]
		}, {
			name: 'Edibles',
			images: [
				'app/images/vegan-oatmeal.jpg',
			]
		}];
	};
})();