(function () {
	'use strict';
	angular.module('app')
		.controller('appController', controller);

	controller.$inject = ['$scope', '$state', '$rootScope', 'ScrollingService', 'NavigationService', 'AuthService'];
	function controller ($scope, $state, $rootScope, ScrollingService, NavigationService, AuthService) {
		$scope.modalInfo = {
			applicationModal: false,
			employmentModal: false,
			productModal: false
		};

		$scope.isLoggedIn = $rootScope.isLoggedIn;
		$scope.productDetails = {
			name: '',
			images: []
		}

		$scope.$on('openModal', (emitInfo, data) => {
			let mi = $scope.modalInfo;
			mi.employmentModal = true;
			if (data.name) {
				data.images.forEach((path) => {
					$scope.productDetails.images.push(path); 
				});
				$scope.productDetails.name = data.name;
			}
			console.log('DATA:',data);
			console.log('EMITINFO:',emitInfo);
			console.log('PRODUCTDETAILS', $scope.productDetails);
		});

		$scope.$watch(() => {
			return $state.$current.name;
		}, () => {
			$scope.isLoggedIn = AuthService.isLoggedIn();
			NavigationService.closeMenu();
			ScrollingService.scrollToTop();
		});

		$scope.closeModal = (event, pd) => {
			console.log(pd);
			const elementClasses = ['The Reserve-close-icon', 'The Reserve-overlay'];
			const isTargetElement = elementClasses.some((className) => {
				return  event.target.className.includes(className);
			});
			$scope.productDetails = {
				name: '',
				images: []	
			}
			if (isTargetElement) {
				const mi = $scope.$parent.modalInfo;
				mi.productModal = mi.applicationModal = mi.employmentModal = false;
			}
		};
	}	

})();