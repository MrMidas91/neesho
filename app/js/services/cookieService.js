(function () {
	'use strict';

	angular.module('CookieService', [])
		.service('CookieService', service);

	function service () {
		const service = this;

		service.getCookie = (cname) => {
			const name = `${cname}=`;
			const decodedCookie = decodeURIComponent(document.cookie);
			const ca = decodedCookie.split(';');
			for(let i = 0; i <ca.length; i++) {
			  let c = ca[i];
			  while (c.charAt(0) == ' ') {
				c = c.substring(1);
			  }
			  if (c.indexOf(name) == 0) {
				return {[c.split("=")[0]]: c.split("=")[1]};
			  }
			}
			return "";
		}

		service.setCookie = (cname, cvalue) => {
			const now = new Date();
			let time = now.getTime();
			time += 3600 * 1000;
			now.setTime(time);
			document.cookie = `${cname}=${cvalue}; domain=.us-east-2.compute.amazonaws.com; expires=${now.toUTCString()}; path=/`;
		}

		service.deleteAllCookies = () => {
			document.cookie.split(';')
				.forEach((cookie) => {
					this.deleteCookie(cookie.split('=')[0]);
				});
		}

		service.deleteCookie = (cname) => {
			document.cookie = `${cname}= ; expires = Thu, 01 Jan 1970 00:00:00 GMT`;
		}

	}	
})();