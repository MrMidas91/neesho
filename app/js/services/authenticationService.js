(function () {
	'use strict';

	angular.module('AuthService', [])
		.service('AuthService', service);
    service.$inject = ['$http', '$state', '$rootScope', 'CookieService'];
	function service ($http, $state, $rootScope, CookieService) {
        const apiBaseUrl = 'https://qb3chnoeze.execute-api.us-east-2.amazonaws.com/Production/';
        $rootScope.isLoggedIn = CookieService.getCookie('isLoggedIn') || false;

        let user = {
            clientId: '',
            access: 'gen'
        }

        this.login = function (creds) {
            return new Promise((resolve, reject) => {
                $http.post(apiBaseUrl + 'login', creds)
                    .then( (res) => {
                        const { data } = res;
                        if (data.hasOwnProperty('failure')) {
                            alert(data.failure);
                            CookieService.getCookie('isLoggedIn') && CookieService.deleteCookie('isLoggedIn');
                            $rootScope.isLoggedIn = false;
                            resolve(true);
                            return;
                        }
                        $rootScope.isLoggedIn = true;
                        user = data;
                        if (data.clientId !== 'gen') {
                            $rootScope.isPresident = true;
                        }
                        resolve(true);
                    })
                    .catch((e) => {
                        alert('There was an error submitting your request. Please close the browser and try again.');
                        CookieService.getCookie('isLoggedIn') && CookieService.deleteCookie('isLoggedIn');
                        $rootScope.isLoggedIn = false;
                        reject('There was an error submitting your request. Please close the browser and try again.');
                    });

            });
        }

        this.isLoggedIn = function () {
            return $rootScope.isLoggedIn;
        }

        this.logOut = function () {
            $rootScope.isPresident = false;
            $rootScope.isLoggedIn = false;
            _isLoggedIn = false;
            $state.go('login');
        }

        this.setUrl = function (url) {
            // urlToNavigateTo = url;
        }

        this.getUser = function () {
            return user;
        }

	}	
})();